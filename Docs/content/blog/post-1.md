---
title: "Resultados"
date: 2021-05-10T15:40:24+06:00
#author
author : ["N3S"]
# categories

# meta description
description: "This is meta description"
# save as draft
draft: false
---
## Conclusões

Ao final, o projeto desenrolou de forma fluída, conseguimos finalizar o que foi previsto e adicionar alguns extras. Nota-se a importância de atividades realizadas antes do início do projeto, como o Brainstorm, onde colocamos várias ideias no papel e selecionamos a mais viável em relação a utilidade, tempo disponível e dificuldade. As divisões foram feitas conforme as aptidões de cada um dos envolvidos, complementando-se. 

Vale ressaltar também a importância de alguém para liderar a equipe. Com a liderança, foi possível uma distribuição melhor das tarefas e tempo, avaliando o que cada um dos alunos poderia fazer de forma mais eficiente com o fim de entregar a atividade dentro do prazo estipulado. Basicamente funcionando como um centralizador das ideias e tomadas de decisões.

## Dificuldades

Dentre as dificuldades destacadas pelo grupo, uma das mais relevantes foi em questão aos prazos. O projeto foi realizado em pouco menos de uma semana, ao mesmo tempo em que havia a necessidade de estudar para provas de outras matérias e entregar atividades. 

O primeiro contato com linguagens como html e css, assim como javascript dificultou a personalização do conteúdo no começo, mas acabamos por superar essa dificuldade manipulando o que já havia sido feito e com lógica. Além disso, a própria criação do site em si levantou algumas dificuldades, devido a ferramentas novas e não antes exploradas.

Alguns dos discentes também tiveram seu primeiro contato com o desenvolvimento de um projeto com uma linguagem orientada a objetos, o que, a princípio, também foi uma dificuldade.

Outro dos grandes problemas se encontrou na plataforma Tinkercad. A plataforma possui muitas complicações, de desempenho a funcionalidades, nosso projeto ficou muito limitado devido a plataforma. A intenção inicial era criar uma centralizadora controlada remotamente que possibilitaria a integração de vários dispositivos de forma mais eficiente. Porém, devido a ausência de materiais chave como módulos de radiofrequência e módulo de internet. No final, acabamos optando por utilizar o sensor infravermelho para controle remoto do dispositivo. 

Ainda quanto a plataforma, a simulação as vezes levam tempos absurdos para rodar, simulando cada segundo equivalente a valores próximos a 20 segundos na vida real, o que dificultou bastante os testes.

Não houveram muitos problemas relacionados ao código, embora a princípio houvesse certa preocupação em como trabalhar com várias pessoas dentro do mesmo código. Desafio este também superado de forma tranquila. Liberando mais tempo para as outras tarefas.

## Video simulação

{{< youtube Lacx-E95xmI >}}

Como visto no vídeo, para começar a configurar o dispositivo é preciso mudar o switch do arduino para ele receber o sinal do IR, porém isso é apenas necessário no simulador, visto que quando posto em prática no mundo físico o sinal irá ser detectado apenas pelo arduino que o controle está sendo apontado.

Para configurar o dispositivo, deve-se apertar o botão "FUNC/STOP" (também é possível resetar o aparelho segurando o botão que está no arduino), após isso, apertar o botão entre 1 e 9 para escolher a tecla que ativará o dispositivo. No vídeo, é escolhido o botão 8. Logo em seguida, é selecionado qual modo será usado para o dispositivo sendo configurado, no caso do vídeo é escolhido o modo de pulso, tecla 1.

Depois do primeiro dispositivo ter sido configurado,  o switch é mudado dele para ele não receber mais o sinal do controle IR, repete-se os mesmos passos do primeiro dispositivo, dessa vez é escolhido o botão 6 para ser o botão que ativa o dispositivo, e é escolhido o modo de alternar estado, tecla 2.

Após configurar os dois dispositivos, é mostrado como é feito o reset de um deles, que é apertando o botão power e colocando a senha de confirmação, que por padrão é 1234. Depois de resetar o aparelho, é escolhida uma nova tecla, 4, para ser o botão que ativa o dispositivo. Dessa vez, é mostrado o
modo pulso com senha, tecla 3. Nesse modo, é necessário escolher uma senha para acionar a função do dispositivo, no caso do vídeo, é escolhida a senha 1111.

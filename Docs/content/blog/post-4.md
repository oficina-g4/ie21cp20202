---
title: "Materiais utilizados"
date: 2021-05-10T15:40:24+06:00
# author
author : ["N3S"]
# categories
# meta description
description: "This is meta description"
# save as draft
draft: f
---
**BIBLIOTECAS**

* **EEPROM**       Lendo e gravando para armazenamento ¨permanente¨

* **IRremote**     Biblioteca para decodificar sensores de infravermelho

* **LiquidCrystal**   Controlando telas de cristal líquido

* **keypad**         Permite ler cliques no botão do teclado 

* **Neopad**         Controlando LEDs NeoPixel

* **Servo**          Controlando servomotores

* **SoftwareSerial** Permitir comunicação em série em outros pinos digitais do Arduino

* **Wire**           Esta biblioteca permite comunicação com dispositivos I2C/TWI

* **SD**             A biblioteca SD permite ler e gravar em placas SD

* **SPI**            Comunicação com dispositivos usando o barramento SPI(Serial Peripheral 									Interface)

* **Stepper**        Controlando motores de passo

**EQUIPAMENTOS**

COMPONENTES                   | QUANTIDADES
------------------------------|-------------
Arduino	                      |      2			
Baterias 9v                   |      2
Lâmpadas                      |      2
Relé SPDT                     |      2
Botões                        |      2				       
Visor de sete segundos        |      2
Resitor 220  Ω                |      6        
Resisistor 1kΩ                |      2        
Interruptor Deslizante        |      2
Sensor de infravermelho       |      2
Controle remoto infravermelho |      1
-------------------------------------------

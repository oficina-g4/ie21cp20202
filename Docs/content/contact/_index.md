---
title: "Objetivos"
bg_image: "images/2020-landscape-2.png"
# meta description
description: "This is meta description"
# save as draft
draft: false
---

## Objetivos

O Not So Smart Switch(N3S) tem por objetivo a automatização de processos residenciais, dentre eles, de forma remota, controle da iluminação e ventilação, controle de portas/portões, com ou sem senha, controle de persianas e dispositivos eletroeletrônicos em geral. Apesar de possuir um funcionamento simples, se baseando no controle da alimentação elétrica por relé, as aplicações são diversas, como já foi dito.

Essa automação visa facilitar a vida do usuário que, no conforto de seu assento pode realizar o controle de diversas áreas do dia a dia.

A automação residencial também tem outros propósitos, mas que começam a inclinar para a área de IoT `Internet of things`, ou Internet das coisas em português. Temos a intenção de aprimorar o projeto, partindo para o IoT, onde será possível realizar a comunicação entre todos os dispositivos por meio da internet, sendo possível fazer controle temporizada e muito mais, que não foram possíveis devido a limitações do Arduino.

Falaremos mais sobre na página [Resultados](https://oficina-g4.gitlab.io/ie21cp20202/blog/post-1/)

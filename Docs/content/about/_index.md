---
title: "Materiais Utilizados"
# meta description
description: "This is meta description"
# save as draft
draft: false
---

**BIBLIOTECA/EQUIPAMENTOS**

* **EEPROM**       Lendo e gravando para armazenamento ¨permanente¨

* **IRremote**     Biblioteca para decodificar sensores de infravermelho

* **LiquidCrystal**   Controlando telas de cristal líquido

* **keypad**         Permite ler cliques no botão do teclado 

* **Neopad**         Controlando LEDs NeoPixel

* **Servo**          Controlando servomotores

* **SoftwareSerial** Permitir comunicação em série em outros pinos digitais do Arduino

* **Wire**           Esta biblioteca permite comunicação com dispositivos I2C/TWI

* **SD**             A biblioteca SD permite ler e gravar em placas SD

* **SPI**            Comunicação com dispositivos usando o barramento SPI(Serial Peripheral 									Interface)

* **Stepper**        Controlando motores de passo

* **2 Arduino**					

* **2 Baterias 9v**				       

* **2 Lâmpadas**					

* **2 Relé SPDT**					 

* **2 Botões**

* **2 Visores de sete segmentos**

* **6 Resistores 220 Ω**

* **2 Resistores 1 KΩ**

* **2 Interruptores deslizante**

* **2 Sensores de infravermelho**

* **remoto**
* **1 Controle infravermelho**

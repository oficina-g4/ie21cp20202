# Introdução
O N3S(Not So SmartSwitch), é um dispositivo compacto com o propósito de facilitar o sistema de automação residencial, tornando dispositivos e componentes elétricos capaz de serem 		controlados através de um  simples controle, assim podendo porporcionar mais conforto e agilidade nas atividades residencias.
## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação citados abaixo e avaliado pelo professor [Jeferson Lima](https://gitlab.com/jeferson.lima):

|Nome| gitlab user|
|---|---|
|Satil Pereira|@satil_pereira|
|Rafael Inácio|@rafaelinacio829|
|Fernando Gnoatto|@fernando_gnoatto|
|Pedro Santiago|@MisterPedro|
|Thomas Delfs|@Thommasdel|

# Documentação

A documentação do projeto pode ser acessada pelo link:

> https://oficina-g4.gitlab.io/ie21cp20202

# Manual de funcionamento 
- Controle > Arduino > Aciona o relê
- Ao usuário ligar o dispositivo, o display de 7 segmentos ligará com o movimento de carregamento. Nesse momento deve ser precionado o botão Func/stop do controle.
O led vermelho liga, esperando uma função que deve ser dada de 1 a 9 do controle, se informado outro valor, ocorre um erro, que é mostrado no display, mas tendo a opção do reset,feito no botão ao lado do relê.
- Quando informado um número valido, o mesmo aparece no display, enquanto o led pisca e volta a ser aceso esperando a função ser completa. 

### Segunda etapa de configuração: Contém as seguintes opções.
1- Pulso (Estado NF(Normamente Fechado)o pulso abrirá o contato do relé).

2- Troca de estado Altera entre NA(Normamente Aberto) e NF(Normalmente Fechado) iniciando em NA.

3- Senha: Uma senha de 4 dígitos será solicitada para dar um pulso (opção 1).

3.1-Quando selicionada a opção 3, o caractere 0 juntamente com o ponto,será mostrado no display. O usuário deverá digitar 4 dígitos que serão configurados na memória desse receptor. (Salvar uma senha).

- Todas as opções acima, devem ser selecionadas no controle infravermelho.

### Após selecionado uma das opções o receptor:

Ele devolverá um feedback visual da opção selecionada.

1- Pulso longo e dois pulsos rápidos.

2- 3 pulsos rápidos.

3- 1 pulso rápido. 

Desta forma a configuração se encerra  e o número configurado será mostrado no display sem o ponto e o LED ficará apagado.

Feito isso para utilizar o dispositivo deve-se digitar o número que está sendo mostrado no display.

Ao receber o sinal o led oscila rapidamente informado o recebimento do sinal. 

Caso tenha sido configurado o opção (3), além de digitar o número configurado no display, deverá digitar a senha configurada.

RESET: O botão power será configurado para o reset. a senha padrão é 1234, aperte power(O LED ficará aceso) e no controle digite a senha (1234) que o reset será efetuado, ou segure o botão(ao lado do  relê) do receptor por 5 segundos; 

## Observações:

- Recomendamos um switch(alimentador do sensor IR(InfraRed//Infravermelho) de cada vez, a fim de programar apenas um arduino de cada vez. A não ser que deseje configurar os dois no mesmo botão.

 - O switch apenas desativa o receptor IR para fins de simulação.










# Links Úteis

* [Kit de Desenvolvimento Arduino](https://www.arduino.cc/)
* [Sintax Markdown](https://docs.gitlab.com/ee/user/markdown.html)
* [Hugo Website](https://gohugo.io/)

